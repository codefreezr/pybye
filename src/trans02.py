#!/usr/bin/env python
import sys
import googletrans

from googletrans import Translator

reload(sys)
sys.setdefaultencoding('utf-8')


translator = Translator()

translated = translator.translate(
    'Meine Gedanken sind bei Euch!', src='de', dest='en')

print(translated.text)


mydict = {
    "Kurmandschi": "ku",
    "Turkish": "tr",
    "German": "de",
    "Romanian": "ro",
    "Bulgarian": "bg",
    "hebrew": "he",
    "Bosnian": "bs",
    "Pashto": "pt",
    "persian": "fa"
}
for mylang, myshort in mydict.items():
    print(mylang)
    translated = translator.translate(
        'Meine Gedanken sind bei Euch!', src='de', dest=myshort)
    print(translated.text)
