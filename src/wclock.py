#!/usr/bin/env python

import datetime

LOCAL_TIMEZONE = datetime.datetime.now(datetime.timezone(datetime.timedelta(0))).astimezone().tzinfo
mystring = str(LOCAL_TIMEZONE) + ": "

from datetime import datetime, timedelta

wtag = [
    "Montag",
    "Dienstag",
    "Mittwoch",
    "Donnerstag",
    "Freitag",
    "Samstag",
    "Sonntag"
]

a = datetime.now()
wd = datetime.today().weekday()
cw = a.isocalendar()[1]
print ("\___")
print (mystring + str(a) + ", " + wtag[wd] + " in der Kalenderwoche: " + str(cw))
print ("---")
print ('Los Angeles \t{:%H:%M:%S}'.format(a + timedelta(hours=-9)))
print ('New York \t{:%H:%M:%S}'.format(a + timedelta(hours=-6))) 
print ('Rio de Janeiro \t{:%H:%M:%S}'.format(a + timedelta(hours=-4)))
print ('London  \t{:%H:%M:%S}'.format(a + timedelta(hours=-1))) 
print ('Berlin  \t{:%H:%M:%S}'.format(a))
print ('Moskau  \t{:%H:%M:%S}'.format(a + timedelta(hours=2))) 
print ('Neu Dehli \t{:%H:%M:%S}'.format(a + timedelta(hours=4)))
print ('Peking  \t{:%H:%M:%S}'.format(a + timedelta(hours=7)))
print ('Tokyo    \t{:%H:%M:%S}'.format(a + timedelta(hours=8)))
print ('Sydney  \t{:%H:%M:%S}'.format(a + timedelta(hours=9)))
print ('Auckland \t{:%H:%M:%S}'.format(a + timedelta(hours=11)))

